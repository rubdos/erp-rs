use router::Router;
use ice_mvc::Routable;

mod controllers;

pub fn routes() -> Router {
    let mut router = Router::new();

    controllers::InvoiceController::add_routes(&mut router);
    controllers::SellableProductController::add_routes(&mut router);
    controllers::Welcome::add_routes(&mut router);

    router
}
