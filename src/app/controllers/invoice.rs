use std::time::SystemTime;

use iron;
use params::{Value};

use diesel::prelude::*;
use diesel::{insert,update};
use ice_mvc::*;

use bigdecimal::BigDecimal;

use schema::*;
use models::{Client, SalesInvoice, SalesInvoiceLine, NewSalesInvoice, NewSalesInvoiceLine, SellableProduct};
use establish_connection;

use helpers;

controller!(
    name: InvoiceController,
    views: "invoice",
    methods: {
        "invoice" => index,
        "invoice/:id" => get,
        "invoice/:id/:number.pdf" => pdf,
        "invoice/new" => new,
        "invoice/new" => create (POST),
    },
    layout: default
);

impl InvoiceController {
    fn new(&mut self, _: &mut iron::Request) {
        let connection = establish_connection();
        let clients: Vec<Client> = clients::table
            .select(clients::all_columns)
            .order(clients::name)
            .load(&connection)
            .expect("Error loading clients");
        self.set("clients", clients);
    }

    fn create(&mut self, _: &mut iron::Request) {
        let connection = establish_connection();
        let customer = match self.query.get("customer") {
            Some(&Value::String(ref id)) => id.parse().expect("Customer id is not integral"),
            _ => -1,
        };
        let customer: Client = clients::table
            .filter(clients::client_id.eq(customer))
            .first(&connection)
            .expect("Could not load customer");
        // Create invoice
        let si = NewSalesInvoice {
            client_id: customer.client_id,
        };

        let products: Vec<SellableProduct> = match self.query.get("product") {
            Some(&Value::Array(ref a)) => a.iter().map(|product_id| {
                match product_id {
                    &Value::String(ref id) => id.parse().expect("Product id not numeric"),
                    _ => -1,
                }
            }).map(|ref product_id| {
                sellable_products::table
                    .filter(sellable_products::sellable_product_id.eq(product_id))
                    .first(&connection)
                    .expect("Could not load product")
            }).collect(),
            _ => vec![],
        };
        let descriptions: Vec<String> = match self.query.get("description") {
            Some(&Value::Array(ref a)) => a.iter().map(|description| {
                match description {
                    &Value::String(ref description) => description.clone(),
                    _ => panic!("Unparsable description."),
                }
            }).collect(),
            _ => vec![],
        };
        let amounts: Vec<i32> = match self.query.get("amount") {
            Some(&Value::Array(ref a)) => a.iter().map(|amount| {
                match amount {
                    &Value::String(ref amount) => amount.parse().unwrap(),
                    _ => panic!("Unparsable amount"),
                }
            }).collect(),
            _ => vec![],
        };
        let prices: Vec<BigDecimal> = match self.query.get("price") {
            Some(&Value::Array(ref a)) => a.iter().map(|price| {
                match price {
                    &Value::String(ref price) => price.parse().unwrap(),
                    _ => panic!("Unparsable price."),
                }
            }).collect(),
            _ => vec![],
        };
        let mut si = insert(&si).into(sales_invoices::table)
            .get_result::<SalesInvoice>(&connection)
            .expect("Could not create invoice");
        let lines: Vec<_> = (0..products.len()).map(|i| {
            let ref product = products[i];
            let description = descriptions[i].clone();
            let amount = amounts[i].clone();
            let price = prices[i].clone();
            NewSalesInvoiceLine {
                sales_invoice_id: si.sales_invoice_id,
                price: price,
                vat_id: 1,
                description: description,
                amount: amount,
                sellable_product_id: product.sellable_product_id,
            }
        }).collect();
        let lines = insert(&lines).into(sales_invoice_lines::table)
            .get_results::<SalesInvoiceLine>(&connection)
            .expect("Could not add invoice lines. This means the database is now corrupt. Good luck.");
        info!("Generating PDF");
        si.generate_pdf(&customer, &lines);
        si = update(&si)
            .set(sales_invoices::pdf.eq(&si.pdf))
            .get_result::<SalesInvoice>(&connection)
            .expect("Could not store PDF in DB");
        self.set("invoice", si);
    }

    fn index(&mut self, _: &mut iron::Request) {
        let connection = establish_connection();
        let results = sales_invoices::table
            .inner_join(clients::table)
            .select((sales_invoices::all_columns, clients::all_columns))
            .order(sales_invoices::sales_invoice_id.desc())
            .load::<(SalesInvoice, Client)>(&connection)
            .expect("Error loading invoices");
        let results = results.into_iter().map(|(invoice, client)| {
            let lines: Vec<SalesInvoiceLine> = SalesInvoiceLine
                ::belonging_to(&invoice)
                .order(sales_invoice_lines::line_id.asc())
                .load(&connection)
                .expect("Error loading lines");
            let (total, total_vat, total_with_vat) = invoice.calculate_amounts(&lines);
            let (total, total_vat, total_with_vat) = (
                helpers::format_price(&total),
                helpers::format_price(&total_vat),
                helpers::format_price(&total_with_vat),
            );
            squash_model! {
                invoice,
                {
                    client,
                    total,
                    total_vat,
                    total_with_vat
                }
            }
        }).collect::<Vec<_>>();
        self.set("invoices", results);
    }

    fn pdf(&mut self, req: &mut iron::Request) {
        let id: i32 = req.extensions.get::<::router::Router>().unwrap().find("id").unwrap().parse().unwrap();
        let connection = establish_connection();
        let (mut invoice, client): (SalesInvoice, Client) = sales_invoices::table
            .inner_join(clients::table)
            .filter(sales_invoices::sales_invoice_id.eq(id))
            .first(&connection)
            .expect("Can't find invoice");
        if invoice.pdf.is_none() {
            let lines: Vec<SalesInvoiceLine> = SalesInvoiceLine
                ::belonging_to(&invoice)
                .order(sales_invoice_lines::line_id.asc())
                .load(&connection)
                .expect("Error loading lines");
            invoice.generate_pdf(&client, &lines);
            invoice = update(&invoice)
                .set(sales_invoices::pdf.eq(&invoice.pdf))
                .get_result::<SalesInvoice>(&connection)
                .expect("Could not store PDF in DB");
        }
        use iron::headers::ContentType;
        use iron::mime::{Mime, TopLevel, SubLevel};
        let mut r = iron::Response::with((iron::status::Ok, invoice.pdf.unwrap()));
        r.headers.set(ContentType(Mime(TopLevel::Application, SubLevel::Ext("pdf".into()), vec![])));
        self.response = Some(r);
    }

    fn get(&mut self, req: &mut iron::Request) {
        let id: i32 = req.extensions.get::<::router::Router>().unwrap().find("id").unwrap().parse().unwrap();
        let connection = establish_connection();
        let (invoice, client): (SalesInvoice, Client) = sales_invoices::table
            .inner_join(clients::table)
            .filter(sales_invoices::sales_invoice_id.eq(id))
            .first(&connection)
            .expect("Can't find invoice");
        let lines: Vec<SalesInvoiceLine> = SalesInvoiceLine
            ::belonging_to(&invoice)
            .order(sales_invoice_lines::line_id.asc())
            .load(&connection)
            .expect("Error loading lines");
        let (total, total_vat, total_with_vat) = invoice.calculate_amounts(&lines);

        self.set("invoice", invoice);
        self.set("client", client);
        self.set("lines", lines);
        self.set("total_amount",   helpers::format_price(&total));
        self.set("total_vat",      helpers::format_price(&total_vat));
        self.set("total_with_vat", helpers::format_price(&total_with_vat));
    }
}

