use std::ops::Index;

use iron;
use iron::prelude::*;
use diesel::prelude::*;
use ice_mvc::*;

use params::{Value};

use schema::*;
use models::{SellableProduct};
use establish_connection;

use helpers;

controller!(
    name: SellableProductController,
    views: "sellable_product",
    methods: {
        "sellable_products" => index
    },
    layout: default
);

impl SellableProductController {
    fn index(&mut self, req: &mut iron::Request) {
        let conn = establish_connection();

        let products = sellable_products::table
            .select(sellable_products::all_columns);
        let products: Vec<SellableProduct> = match self.query.get("q") {
            Some(&Value::String(ref q)) => {
                let q = "%".to_string() + &q + "%";
                products.filter(sellable_products::description.like(q))
                    .load(&conn)
                    .expect("Could not load products")
            },
            _ => {
                products.load(&conn)
                    .expect("Could not load products")
            },
        };
        self.set("products", products);
    }
}
