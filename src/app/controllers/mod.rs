mod invoice;
pub use self::invoice::*;

mod sellable_product;
pub use self::sellable_product::*;

use iron;

controller!(
    name: Welcome,
    views: "welcome",
    methods: {
        "" => index,
    },
    layout: default
);

impl Welcome {
    fn index(&mut self, _: &mut iron::Request) {
    }
}
