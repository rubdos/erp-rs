extern crate erp;
extern crate bigdecimal;
#[macro_use]
extern crate diesel;
extern crate chrono;
#[macro_use]
extern crate log;
extern crate env_logger;

extern crate clap;

extern crate bank;

use std::path::Path;
use std::fs::{File};
use std::io::{BufReader};
use std::fmt::Display;

use clap::{Arg, App};

use diesel::pg::PgConnection;
use diesel::prelude::*;

use erp::models::{BankTransaction, SalesInvoice, SalesInvoiceLine};

enum StoreStatus {
    Exists,
    Added,
    Error,
}

fn store_transaction(transaction: Box<bank::Transaction>, connection: &PgConnection) -> StoreStatus {
    let t = BankTransaction {
         reference: transaction.get_identifier().to_string(),
         settlement_date: transaction.settlement_date().naive_utc(),
         date: transaction.date().naive_utc(),
         amount: transaction.amount().clone(),
         currency: transaction.currency().to_string(),
         counterparty_iban: transaction.counterparty_account().to_string(),
         counterparty_name: transaction.counterparty_name().to_string(),
         messages: vec![
             transaction.first_message().to_string(),
             transaction.second_message().to_string(),
         ],
         origin_iban: transaction.origin_account().to_string(),
    };

    use erp::schema::bank_transactions;
    use diesel::result::DatabaseErrorKind;
    use diesel::result::Error::DatabaseError;
    match diesel::insert(&t)
        .into(bank_transactions::table)
        .get_result::<BankTransaction>(connection) {
            Ok(transaction) => {
                debug!("Inserted {}", transaction.reference);
                StoreStatus::Added
            },
            Err(DatabaseError(DatabaseErrorKind::UniqueViolation,e)) => {
                debug!("Could not insert transaction {}: {:?}", t.reference, e);
                StoreStatus::Exists
            }
            Err(e) => {
                error!("Could not insert {}: {:?}", t.reference, e);
                StoreStatus::Error
            }
    }
}

fn update_payment_status(conn: &PgConnection) {
    info!("Updating payment status");
    use erp::schema::{bank_transactions, sales_invoices, sales_invoice_lines};
    use diesel::result::DatabaseErrorKind;
    use diesel::result::Error::DatabaseError;

    let unpaid_invoices: Vec<SalesInvoice> = sales_invoices::table
        .select(sales_invoices::all_columns)
        .filter(sales_invoices::date_paid.is_null())
        .load(conn)
        .expect("Cannot load unpaid invoices");

    for invoice in unpaid_invoices {
        info!("Updating payment status for invoice {}", invoice.invoice_number());
        let lines: Vec<SalesInvoiceLine> = SalesInvoiceLine
            ::belonging_to(&invoice)
            .order(sales_invoice_lines::line_id.asc())
            .load(conn)
            .expect("Error loading lines");
        let (_, _, amount_to_pay) = invoice.calculate_amounts(&lines);

        debug!("SEPA RF for {}: {}", invoice.invoice_number(), invoice.calculate_sepa_rf());
        debug!("Belgian OGM for {}: {}", invoice.invoice_number(), invoice.calculate_belgian_ogm());

        let payments: Vec<BankTransaction> = bank_transactions::table
            .select(bank_transactions::all_columns)
            .filter(bank_transactions::amount.eq(amount_to_pay))
            .load(conn)
            .expect("Error loading payments");
        let payments = payments.into_iter().filter(|p| {
            p.messages.iter().any(|x| {
                x.contains(&invoice.invoice_number())
                || x.contains(&invoice.calculate_sepa_rf().to_string())
                || x.contains(&invoice.calculate_belgian_ogm().to_string())
            })
        }).collect::<Vec<_>>();
        debug!("Payments for {}: {:?}", invoice.invoice_number(), payments);
        if payments.len() == 1 {
            info!("Found one payment for invoice {}:", invoice.invoice_number());
            let res = diesel::update(sales_invoices::table.filter(sales_invoices::sales_invoice_id.eq(invoice.sales_invoice_id)))
                .set(sales_invoices::date_paid.eq(Some(payments[0].date)))
                .get_result::<SalesInvoice>(conn);
            match res {
                Ok(_) => info!("Updated payment status for invoice."),
                Err(e) => error!("Could not update payment status for invoice: {}", e),
            }
        } else if payments.len() > 1 {
            error!("Multiple payments found for {}, please check.", invoice.invoice_number());
        } else {
            let due = std::time::SystemTime::now().duration_since(invoice.date).unwrap();
            warn!("Invoice {} is {} days due.", invoice.invoice_number(), due.as_secs()/3600/24);
        }
    }
}

fn main() {
    let matches = App::new("Bank file importer")
        .arg(Arg::with_name("import-directory")
             .short("i")
             .long("directory")
             .value_name("DIRECTORY")
             .takes_value(true))
        .get_matches();

    env_logger::init().unwrap();

    let dir = if let Some(dir) = matches.value_of("import-directory") {
        info!("Import from {}", dir);
        Path::new(dir)
    } else {
        info!("Import from current directory");
        Path::new(".")
    };

    if !dir.is_dir() {
        error!("No such directory: {:?}", dir);
        return;
    }

    let dir = std::fs::read_dir(dir).unwrap();

    let connection = erp::establish_connection();

    let dir = dir.flat_map(|entry| {
        entry.and_then(|entry| {
            info!("Visting {:?}", entry.path());
            let f = File::open(entry.path());
            let reader = BufReader::new(f?);
            bank::read_transaction_file(reader)
        })
    }).flat_map(|mut x| {
        x.read_transactions().collect::<Vec<_>>()
    })
    .map(|transaction| {
        let id = transaction.get_identifier().to_string();
        let status = store_transaction(transaction, &connection);
        match status {
            StoreStatus::Exists => {
                warn!("Transaction {} already exists in database", id);
            }
            StoreStatus::Added => {
                info!("Transaction {} stored in database", id);
            }
            StoreStatus::Error => {
                error!("Transaction {} NOT stored in database", id);
            }
        }
    }).collect::<Vec<_>>();

    update_payment_status(&connection);
}
