extern crate erp;

#[macro_use]
extern crate log;
extern crate env_logger;
extern crate iron;

extern crate mount;
extern crate staticfile;

extern crate num;

use iron::Iron;
use mount::Mount;

fn main() {
    env_logger::init().unwrap();

    let mut mount = Mount::new();
    let routes = erp::app::routes();
    mount.mount("/", routes);
    let bootstrap = staticfile::Static::new("node_modules/bootstrap/dist/");
    mount.mount("/assets/bootstrap/", bootstrap);
    let images = staticfile::Static::new("images");
    mount.mount("/assets/images/", images);
    let css = staticfile::Static::new("css");
    mount.mount("/assets/css/", css);
    let js = staticfile::Static::new("js");
    mount.mount("/assets/js/", js);

    Iron::new(mount).http("localhost:3000").unwrap();
}
