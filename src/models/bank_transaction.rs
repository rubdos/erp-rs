use chrono::prelude::*;
use bigdecimal::BigDecimal;

use schema::*;

#[derive(Identifiable, Queryable, Associations, Insertable, Debug, Serialize)]
#[table_name="bank_transactions"]
#[primary_key(reference,origin_iban)]
pub struct BankTransaction {
    pub reference: String,
    #[serde(skip_serializing)]
    pub settlement_date: NaiveDate,
    #[serde(skip_serializing)]
    pub date: NaiveDate,
    #[serde(skip_serializing)]
    pub amount: BigDecimal,
    pub currency: String,
    pub counterparty_iban: String,
    pub counterparty_name: String,
    pub messages: Vec<String>,
    pub origin_iban: String,
}
