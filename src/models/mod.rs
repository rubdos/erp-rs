use std;
use liquid;

use ice_mvc::view::IntoLiquid;

use schema::*;

mod sales_invoice;
pub use self::sales_invoice::*;

mod bank_transaction;
pub use self::bank_transaction::*;

#[derive(Identifiable, Queryable, Associations, Serialize)]
#[has_many(sales_invoices)] // Hopefully, has_many ;)
#[primary_key(client_id)]
pub struct Client {
    pub client_id: i32,
    pub name: Option<String>,
    pub street: Option<String>,
    pub city: Option<String>,
    pub postalcode: Option<String>,
    pub vat: Option<String>,
    pub attn: Option<String>,
}

liquid_model!(
    name: Client
    fields: {
        client_id,
        name,
        attn,
        vat,
        city,
        postalcode,
        street
    }
    resource: client
    calculated_fields: {
        printed_name,
        latex_vat
    }
);

impl Client {
    pub fn latex_vat(&self) -> String {
        match self.vat {
            Some(ref vat) => "BTW~".to_string() + &vat,
            None => "".to_string(),
        }
    }
    pub fn printed_name(&self) -> String {
        match self.attn {
            Some(ref attn) => "(t.a.v.~".to_string() + &attn + ")",
            None => match self.vat {
                Some(ref vat) => match self.name {
                    Some(ref name) => name.to_string(),
                    None => unreachable!("Business client has no name."),
                },
                None => "Particuliere klant".to_string(),
            }
        }
    }
}
