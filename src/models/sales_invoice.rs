use std;
use chrono::prelude::*;
use liquid;
use bigdecimal::BigDecimal;
use bank;

use num::PrimInt;

use ice_mvc::view::IntoLiquid;

use schema::*;
use helpers;

use super::Client;

#[derive(Insertable)]
#[table_name="sales_invoices"]
pub struct NewSalesInvoice {
    pub client_id: i32,
}

#[derive(Insertable)]
#[table_name="sales_invoice_lines"]
pub struct NewSalesInvoiceLine {
    pub sales_invoice_id: i32,
    pub price: BigDecimal,
    pub vat_id: i32,
    pub description: String,
    pub amount: i32,
    pub sellable_product_id: i32,
}

#[derive(Identifiable, Queryable, Associations, AsChangeset, Serialize)]
#[belongs_to(Client)]
#[has_many(sales_invoice_lines)]
#[primary_key(sales_invoice_id)]
pub struct SalesInvoice {
    pub sales_invoice_id: i32,
    #[serde(skip_serializing)]
    pub date: NaiveDate,
    pub client_id: i32,
    pub pdf: Option<Vec<u8>>,
    #[serde(skip_serializing)]
    pub date_paid: Option<NaiveDate>,
    pub printed: bool,
}

#[derive(Identifiable, Queryable, Associations, Serialize)]
#[belongs_to(SalesInvoice)]
#[primary_key(line_id)]
pub struct SalesInvoiceLine {
    pub line_id: i32,
    pub sales_invoice_id: i32,
    #[serde(skip_serializing)]
    pub price: BigDecimal,
    pub vat_id: i32,
    pub description: String,
    pub amount: i32,
    pub sellable_product_id: i32,
}

#[derive(Identifiable, Queryable, Associations, Serialize)]
#[has_many(sales_invoice_lines)]
#[primary_key(sellable_product_id)]
pub struct SellableProduct {
    pub sellable_product_id: i32,
    pub description: String,
    pub vat_id: i32,
}

liquid_model!(
    name: SellableProduct
    fields: {
        sellable_product_id,
        description
    }
    resource: sellable_product
);

liquid_model!(
    name: SalesInvoiceLine
    fields: {
        line_id,
        description,
        amount
    }
    resource: sales_invoice_line
    calculated_fields: {
        format_price,
        format_total_price,
        format_vat
    }
);

impl SalesInvoiceLine {
    pub fn format_price(&self) -> String {
        helpers::format_price(&self.price)
    }
    pub fn format_total_price(&self) -> String {
        helpers::format_price(&self.total_price())
    }
    pub fn format_vat(&self) -> String {
        // TODO: This 21 is hardcoded *everywhere*,
        // but it's present in the database.
        // My company only deals in 21%... grep -R 21 should find everything
        format!("€{:.2} (21%)", self.calculate_vat())
    }

    fn total_price(&self) -> BigDecimal {
        use num::{BigInt, FromPrimitive};
        self.price.clone() * BigDecimal::new(BigInt::from_u64(self.amount as u64).unwrap(), 0)
    }
    fn calculate_vat(&self) -> BigDecimal {
        use num::{BigInt, FromPrimitive};
        self.price.clone() * BigDecimal::new(BigInt::from_u64(21).unwrap(), 2)
    }
}

liquid_model!(
    name: SalesInvoice
    fields: {
        sales_invoice_id,
        printed
    }
    resource: invoice
    calculated_fields: {
        invoice_number,
        paid,
        sepa_rf,
        belgian_ogm,
        latex_date
    }
);

impl SalesInvoice {
    pub fn latex_date(&self) -> String {
        self.date.format("%d/%m/%Y").to_string()
    }
    pub fn invoice_number(&self) -> String {
        format!("SI{:04}{:04}",
                self.year(),
                self.sales_invoice_id)
    }
    pub fn paid(&self) -> bool {
        self.date_paid.is_some()
    }
    pub fn year(&self) -> i32 {
        self.date.year()
    }

    pub fn sepa_rf(&self) -> String {
        self.calculate_sepa_rf().to_string()
    }

    pub fn belgian_ogm(&self) -> String {
        self.calculate_belgian_ogm().to_string()
    }

    pub fn calculate_sepa_rf(&self) -> bank::scr::RF {
        let rf = self.sales_invoice_id as u64
            + self.client_id as u64 * 10000;
        let rf = rf.to_string();
        bank::scr::RF::new(&rf).expect(&format!("Could not generate RF number for invoice {}", self.invoice_number()))
    }

    pub fn calculate_belgian_ogm(&self) -> bank::scr::OGM {
        bank::scr::OGM::new((self.year() as u64
                            + self.sales_invoice_id as u64 * 10000
                            + self.client_id as u64 * 100000000) % 10.pow(10))
            .expect(&format!("Could not generate Belgian transaction number for invoice {}", self.invoice_number()))
    }

    pub fn generate_pdf(&mut self, client: &::models::Client, lines: &Vec<SalesInvoiceLine>) {
        use ice_mvc::view::{IntoLiquid, AsLiquid};
        use liquid::Renderable;
        use tempdir::TempDir;
        use std::process::Command;
        use std::fs::File;
        use std::io::{Read, Write};

        let (total_price, total_vat, total_with_vat) = self.calculate_amounts(lines);
        // Initialize template
        let template = ::liquid::parse(::INVOICE_TEMPLATE, Default::default())
            .expect("Could not parse invoice_template");
        let mut context = liquid::Context::new();
        context.set_val("invoice", self.as_liquid());
        context.set_val("client", client.as_liquid());
        context.set_val("lines", lines.as_liquid());
        context.set_val("total_amount",   helpers::format_price(&total_price).into_liquid());
        context.set_val("total_vat",      helpers::format_price(&total_vat).into_liquid());
        context.set_val("total_with_vat", helpers::format_price(&total_with_vat).into_liquid());
        context.add_filter("latex", Box::new(|input, _args| {
            use liquid::{Renderable, Context, Value, FilterError};
            if let &Value::Str(ref s) = input {
              Ok(Value::Str(
                  s.replace('&', "\\&")
                   .replace('%', "\\%")
                   .replace("<->", "$\\leftrightarrow$")
                   .replace('<', "$<$")
                   .replace('>', "$>$")
              ))
            } else {
              Err(FilterError::InvalidType("Expected a string".to_owned()))
            }
        }));

        let contents = template.render(&mut context).unwrap().unwrap();

        // Create files
        if let Ok(dir) = TempDir::new(&format!("invoice-{}", self.invoice_number())) {
            let invoice_path = dir.path().join("invoice.tex");
            let mut f = File::create(invoice_path).unwrap();
            f.write_all(contents.as_bytes()).unwrap();
            f.sync_all().unwrap();

            let invoice_path = dir.path().join("invoice.cls");
            let mut f = File::create(invoice_path).unwrap();
            f.write_all(::INVOICE_CLASS).unwrap();
            f.sync_all().unwrap();

            // Render
            Command::new("latexmk")
                .arg("-pdf")
                //.arg("-interaction").arg("batchmode")
                .arg("invoice.tex")
                .current_dir(dir.path())
                .status()
                .expect("Could not render invoice");
            let mut f = File::open(dir.path().join("invoice.pdf")).unwrap();
            let mut contents = Vec::new();
            f.read_to_end(&mut contents).unwrap();
            self.pdf = Some(contents);
        } else {
            panic!("Cannot create temp dir");
        }
    }

    pub fn calculate_amounts(&self, lines: &Vec<SalesInvoiceLine>)
            -> (BigDecimal, BigDecimal, BigDecimal) {

        use num::{BigInt, FromPrimitive, Zero, ToPrimitive};
        use std::str::FromStr;

        let ten_k = BigDecimal::from_str("1000").unwrap();
        let vat = BigDecimal::new(BigInt::from_u64(21).unwrap(), 2);
        let mut total_price = BigDecimal::zero();
        for line in lines {
            total_price = total_price + line.price.clone()*BigDecimal::new(BigInt::from_u64(line.amount as u64).unwrap(), 0);
        }
        let total_vat = total_price.clone() * vat;
        // TODO: implement rounding in BigDecimal. This is crap.
        let rounded_vat = (total_vat * ten_k).to_u64().unwrap();
        let rounded_vat = match rounded_vat % 10 {
            0...4 => rounded_vat / 10,
            5...9 => rounded_vat / 10 + 1,
            _ => unreachable!("int % 10 > 9. Impossible"),
        };
        let total_vat = BigDecimal::new((rounded_vat).into(), 2);
        let total_with_vat = total_vat.clone() + total_price.clone();
        (total_price, total_vat, total_with_vat)
    }
}
