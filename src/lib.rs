extern crate iron;
extern crate mount;
extern crate router;
#[macro_use]
extern crate ice_mvc;
#[macro_use]
extern crate log;
extern crate liquid;
extern crate staticfile;
extern crate params;
extern crate num;
extern crate bigdecimal;
extern crate tempdir;

#[macro_use]
extern crate diesel_codegen;
#[macro_use]
extern crate diesel;
extern crate dotenv;
extern crate chrono;

extern crate bank;
extern crate serde;
extern crate serde_json;
#[macro_use]
extern crate serde_derive;

pub mod app;

pub mod schema;
pub mod models;

use std::env;
use dotenv::dotenv;
use diesel::prelude::*;
use diesel::pg::PgConnection;

ice_declare_layouts!(default);

pub static INVOICE_TEMPLATE: &'static str = include_str!("latex/invoice.tex.liquid");
pub static INVOICE_CLASS: &'static [u8] = include_bytes!("latex/invoice.cls");

pub fn establish_connection() -> PgConnection {
    dotenv().ok();

    let database_url = env::var("DATABASE_URL")
        .expect("DATABASE_URL must be set");
    PgConnection::establish(&database_url)
        .expect(&format!("Error connecting to {}", database_url))
}

pub mod helpers {
    use bigdecimal::BigDecimal;
    pub fn format_price(price: &BigDecimal) -> String {
        format!("{:.2}", price)
    }
}
