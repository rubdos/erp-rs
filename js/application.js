function client_form_add_product() {
  id = $("#product-table > tbody").children().length;
  row = '<tr>'+
    '<td width="70%"><select id="product-'+id+'" name="product[]" class="form-control"></select>' +
    '<input type="text" name="description[]" class="form-control" placeholder="Description">'+
    '</td>' +
    '<td><div class="input-group"><div class="input-group-addon">#</div><input type="numeric" class="form-control" name="amount[]" step="0.01" value="1" /></div></td>'+
    '<td><div class="input-group"><div class="input-group-addon">€</div><input type="numeric" class="form-control" name="price[]" step="0.01" value="1.00" /></div></td>'+
    '</tr>';
  $("#product-table > tbody:last-child").append(row);
  $("#product-" + id).select2({
    ajax: {
      url: "/sellable_products",
      dataType: 'json',
      delay: 150,
      processResults: function(data, params) {
        console.log(params);
        return {
          results: data.products.map(function(product) {
            return {
              id: product.sellable_product_id,
              text: product.description,
            };
          }),
        };
      },
    }
  });
}

function build_client_form() {
  $("#customer").select2();
  $("#add-product").click(client_form_add_product);
  client_form_add_product();
}
$(document).ready(build_client_form);
